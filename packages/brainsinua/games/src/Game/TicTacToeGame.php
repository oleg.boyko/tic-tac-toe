<?php
namespace brainsinua\games\Game;

use brainsinua\games\Exception\GameException;
use brainsinua\games\Storage;
use brainsinua\games\Field;

class TicTacToeGame
{
    private $name;
    private $id;
    private $storage;
    private $field;
    private $response;

    public function __construct(Storage\StorageInterface $storage, Field\GameField $field)
    {
        $this->storage = $storage;
        $this->field = $field;
        $this->name = 'Game';
        $this->response = array(
            'finished' => false,
            'error' => false
        );
    }

    public function run(array $input)
    {
        switch ($input['mode']) {

            case 'start': {
                    $gameId = $this->startNewGame();
                    $this->addResponse(['gameId' => $gameId]);
                }
                break;

            case 'move': {
                    $coordinates = explode(',', $input['coordinates']);

                    $this->load($input['gameId']);

                    if ($this->isFinished()) {
                        $this->finalActions();
                    } else {
                        list($success, $message) = $this->proccessMove($input['playerRole'], $coordinates);

                        if (!$success) {
                            $this->addResponse([
                                'error' => true,
                                'message' => $message
                            ]);
                        }
                    }
                }
                break;

            default:
                if (!$input['gameId']) {
                    $this->addResponse(['message' => 'Waiting for requests']);
                } else {
                    $this->load($input['gameId']);

                    if ($this->isFinished()) {
                        $this->finalActions();
                    } else {
                        $this->addResponse(['Next player: ' . $this->getNextPlayer()]);
                    }

                    $this->addResponse([
                        'finished' => $this->isFinished(),
                        'field' => $this->renderField()
                    ]);
                }
        }

        return $this->response;
    }

    private function addResponse(array $data)
    {
        $this->response = array_replace($this->response, $data);
    }

    private function finalActions()
    {
        if ($winner = $this->getWinner()) {
            $message = 'Game ended! Winnner is ' . $winner;
        } else {
            $message = 'Game ended and there is no winner';
        }

        $this->addResponse([
            'finished' => true,
            'winner' => $winner,
            'message' => $message
        ]);
    }

    private function renderField()
    {
        $out = '';
        $field = $this->field->getFieldData();

        for ($i = 0; $i < count($field); $i++) {
            for ($j = 0; $j < count($field); $j++) {
                $out .= '[&nbsp;&nbsp;' . (!empty($field[$i][$j]) ? $field[$i][$j] : '&nbsp;&nbsp;') . '&nbsp;&nbsp;]';
            }
            $out .= '<br />';
        }

        return $out;
    }

    private function proccessMove(string $role, array $coordinates)
    {
        try {
            $this->field->makeMove($role, $coordinates);
        } catch (\Exception $e) {
            return [false, $e->getMessage()];
        }
        $this->saveState();

        return [true, ''];
    }

    private function getNextPlayer()
    {
        return $this->field->getNextStepRole();
    }

    private function load(string $gameId)
    {
        $this->setId($gameId);

        try {
            $data = $this->storage->read($this->name);
            $this->field->loadSavedState($data['field']);
            if (!$data) {
                throw new GameException('Saved game not found!');
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
	    exit();
        }
    }

    private function saveState()
    {
        $data['field'] = $this->field->returnCurrentState();

        $this->storage->write($this->name, $data);
    }

    private function getWinner()
    {
        return $this->field->hasWinner();
    }

    private function isFinished()
    {
        return ($this->getWinner() || $this->field->isFilled());
    }

    private function getField()
    {
        return $this->field->getFieldData();
    }

    private function setId($id)
    {
        $this->id = $id;
        $this->name .= $id;
    }

    private function startNewGame()
    {
        $gameId = substr(md5(time()), 0, 10);

        $this->setId($gameId);
        $this->saveState();

        return $gameId;
    }
}
