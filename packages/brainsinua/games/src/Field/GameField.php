<?php

namespace brainsinua\games\Field;

use brainsinua\games\Entity;

abstract class GameField implements Entity\BaseEntityInterface
{
    /*
     * @var fieldData - array of cells
     */
    protected $fieldData;

    abstract public function hasWinner();
}
