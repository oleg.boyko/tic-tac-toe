<?php
require_once('vendor/autoload.php');

use brainsinua\games as Engine;

set_error_handler(function ($severity, $message, $file, $line) {
    throw new \ErrorException($message, $severity, $severity, $file, $line);
});

$input = array(
    'mode' => $_GET['m'] ?? false,
    'gameId' => $_GET['gameId'] ?? false,
    'playerRole' => $_POST['role'] ?? false,
    'coordinates' => $_POST['coords'] ?? false
);

$game = new Engine\Game\TicTacToeGame(new Engine\Storage\JsonFileStorage(dirname(__FILE__) . '/storage/'), new Engine\Field\TicTacToeGameField());

$response = $game->run($input);

if (isset($response['field'])) {
    echo $response['field'];
    echo isset($response['message']) ? $response['message'] : '';
} else {
    http_response_code(200);
    header('Content-type: application/json');
    echo json_encode($response);
    exit();
}
