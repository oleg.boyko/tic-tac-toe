<?php

namespace brainsinua\games\Entity;

interface BaseEntityInterface
{
    public function loadSavedState(string $data);
    
    public function returnCurrentState();
}
